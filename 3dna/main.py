from RotTable import RotTable
from Traj3D import Traj3D
import os, json
import numpy as np
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings("ignore")

from algorithm.algo_gen import AlgoGenetique
import algorithm.croisement as croisement
import algorithm.evaluation as evaluation
import algorithm.mutation as mutation
import algorithm.selection as selection

main_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

def afficher(filepath, plasmid_path):
    print(f"Paramètres : {filepath}")
    print(f"Plasmid : {plasmid_path}")

    rot_table = RotTable()
    rot_table.rot_table = json.load(open(filepath))

    lineList = [line.rstrip('\n') for line in open(plasmid_path)]
    plasmid_seq = ''.join(lineList[1:])

    bf = 2
    plasmid_seq += plasmid_seq[:bf]

    traj = Traj3D()
    traj.compute(plasmid_seq, rot_table)
    xyz = np.array(traj.getTraj())
    x, y, z = xyz[:,0], xyz[:,1], xyz[:,2]

    plt.close()

    ax = plt.axes(projection='3d')

    ax.plot(x, y, z)


    # Points au début, et premier point fictif (normalement aux mêmes endroits)
    ax.plot(x[0], y[0], z[0], "g.")
    ax.plot(x[-bf], y[-bf], z[-bf], "r.")

    # Point juste après le début, et juste avant la fin
    ax.plot(x[1], y[1], z[1], "g+")
    ax.plot(x[-bf-1], y[-bf-1], z[-bf-1], "r+")

    # Vecteur donnant la direction
    if bf >= 2:
        ax.plot([x[0], x[0] + (x[1]-x[0])*20], [y[0],y[0] + (y[1]-y[0])*20], 'g', zs=[z[0],z[0]+ (z[1]-z[0])*20])
        ax.plot([x[-bf], x[-bf] + (x[-bf+1]-x[-bf])*20], [y[-bf],y[-bf] + (y[-bf+1]-y[-bf])*20], 'r', zs=[z[-bf],z[-bf]+ (z[-bf+1]-z[-bf])*20])

    # Points inutiles, pour centrer la figure
    ax.plot(-5000, -5000, -5000, "w+")
    ax.plot(5000, 5000, 5000, "w+")

    plt.show()

def main():

    print("Programme 3dna, EI du 28/01/2023")
    print("par Erwin, Maxence L, Maxence R, Clara, et Julia")
    print("")

    print("Que voulez-vous : afficher un ADN, ou lancer l'algorithme génétique ?")
    operation = input("> Tapez [adn | algo] : ")

    if operation == "adn":
        print("")
        print("Veuillez taper l'emplacement relatif des paramètres (.json).")
        print(f"Nous ajouterons automatiquement le chemin suivant : {main_path}/")
        filepath = input("> Tapez l'emplacement du fichier, ou laissez vide si vous souhaitez prendre la table originale (3dna/table.json) : ")

        if filepath == "" or filepath == " ":
            filepath = "3dna/table.json"

        filepath = os.path.join(main_path, filepath)

        print("")
        print("Veuillez taper l'emplacement du plasmid (.fasta).")
        print(f"Nous ajouterons automatiquement le chemin suivant : {main_path}/")
        plasmid_path = input("> Tapez l'emplacement du plasmid, ou laissez vide si vous souhaitez prendre la plasmid plasmid_8k.fasta : ")

        if plasmid_path == "" or plasmid_path == " ":
            plasmid_path = "data/plasmid_8k.fasta"
        
        plasmid_path = os.path.join(main_path, plasmid_path)
        print("")
        afficher(filepath, plasmid_path)
    
    elif operation == "algo":
        
        print("")
        print("Veuillez taper l'emplacement du plasmid (.fasta).")
        print(f"Nous ajouterons automatiquement le chemin suivant : {main_path}/")
        plasmid_path = input("> Tapez l'emplacement du plasmid, ou laissez vide si vous souhaitez prendre la plasmid plasmid_8k.fasta : ")

        if plasmid_path == "" or plasmid_path == " ":
            plasmid_path = "data/plasmid_8k.fasta"
        plasmid_path = os.path.join(main_path, plasmid_path)
        
        print("")
        taille_population = int(input("> Taille de la population : "))
        g_max_simulation = 2**32

        print("")
        nb_generations = int(input("> Nombre de générations : "))

        print("")
        print("Quelle fonction d'évaluation ?")
        fct_evaluation_string = input("> Tapez [simple | dist_et_orient] : ")

        if fct_evaluation_string == "simple":
            fct_evaluation = evaluation.evaluation_simple
        elif fct_evaluation_string == "dist_et_orient":
            coef1 = float(input("> Coef pour les distances (float, 1 est une bonne valeur) : "))
            coef2 = float(input("> Coef pour les orientations (float, 100 est une bonne valeur) : "))
            fct_evaluation = evaluation.evaluation_dist_orient(coef1, coef2)
        else:
            print(f"Entrée invalide ({fct_evaluation_string})")
            return

        print("")
        print("Quelle fonction de sélection ?")
        fct_selection_string = input("> Tapez [tournoi | tournoi_aleatoire | tournoi_aleatoire_dynamique | elitisme | roulette | roulette_rang | roulette_rang_sansrepet] : ")
        if fct_selection_string == "tournoi":
            fct_selection = selection.tournoi
        elif fct_selection_string == "tournoi_aleatoire":
            proba = float(input("> Probabilité (float) ? "))
            fct_selection = selection.tournoi_aleatoire_fixe(proba)
        elif fct_selection_string == "tournoi_aleatoire_dynamique":
            fct_selection = selection.tournoi_aleatoire_dynamique
        elif fct_selection_string == "elitisme":
            fct_selection = selection.elitisme
        elif fct_selection_string == "roulette":
            fct_selection = selection.roulette
        elif fct_selection_string == "roulette_rang":
            fct_selection = selection.roulette_rang
        elif fct_selection_string == "roulette_rang_sansrepet":
            fct_selection = selection.roulette_rang_sansrepet
        else:
            print(f"Entrée invalide ({fct_selection_string})")
            return
        

        print("")
        print("Quelle fonction de croisement ? On utilisera la méthode des 2 points.")
        fct_croisement_string = input("> Tapez [couple_aleatoire | orgie] : ")
        if fct_croisement_string == "couple_aleatoire":
            fct_croisement = croisement.couple_aleatoire
        elif fct_croisement_string == "orgie":
            nbparents = int(input("> Nombre de parents par orgie :"))
            fct_croisement = croisement.orgie(nbparents)
        else:
            print(f"Entrée invalide ({fct_croisement_string})")
            return
        
        print("")
        print("Quelle mutation ?")
        fct_mutation_string = input("> Tapez [fixe | petits_bits | petits_bits_generation] : ")
        if fct_mutation_string == "fixe":
            proba_mutation = float(input("> Quelle probabilité de mutation (0.001 est une bonne valeur) : "))
            fct_mutation = mutation.aleatoire(proba_mutation)
        elif fct_mutation_string == "petits_bits":
            proba_mutation = float(input("> Quelle probabilité de mutation ? (0.001 est une bonne valeur) : "))
            rapport = float(input("> Quel rapport entre le bit le plus faible et le bit le plus fort : "))
            fct_mutation = mutation.petits_bits(proba_mutation, rapport)
        elif fct_mutation_string == "petits_bits_generation":
            proba_mutation = float(input("> Quelle probabilité de mutation ? (0.001 est une bonne valeur) : "))
            rapport = float(input("> Quel rapport entre le bit le plus faible et le bit le plus fort : "))
            rapport_gen = float(input("> Quel rapport de génération : "))
            fct_mutation = mutation.generation(proba_mutation, rapport, rapport_gen, nb_generations)
        else:
            print(f"Entrée invalide({fct_mutation_string})")
            return

        parametres_arret = [50, 0.01]

        resultat = AlgoGenetique(fct_evaluation, fct_selection, fct_croisement, fct_mutation, 
                                taille_population, g_max_simulation, nb_generations, plasmid_path, 2, parametres_arret)
    else:
        print(f"Entrée invalide ({operation})")
        return

if __name__ == "__main__":
    main()