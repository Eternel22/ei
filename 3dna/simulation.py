import algorithm.croisement as croisement
import algorithm.selection as selection
import algorithm.mutation as mutation
import algorithm.evaluation as evaluation

from algorithm.algo_gen import AlgoGenetique
import numpy as np

def fct_inutile(self, generation): return ()

def comparaison_type_selection(liste_fct_eval, liste_selection, liste_croisement, liste_mutation, liste_taille_population, liste_gmax, liste_limite, nb_occurences):
    """ Cette fonction prend en entrée des listes d'arguments d'AlgoGenetique ainsi qu'une variable
        nb_occurences qui correspond au nombre de fois que l'algorithme est exécuté pour faire une moyenne
        du meilleur score dans chaque cas. """
    l = len(liste_croisement)
    liste_des_meilleurs_scores = []
    for i in range(l):
        moy = 0
        for j in range(nb_occurences):
            AlgoGen = AlgoGenetique(liste_fct_eval[i], liste_selection[i], liste_croisement[i],
                                    liste_mutation[i], liste_taille_population[i], liste_gmax[i], liste_limite[i])
            best_score = AlgoGen.best_individu.score
            moy += best_score
        print("Le meilleur score moyen de la fonction " +
              liste_selection[i].__name__ + " est :")
        print(moy/nb_occurences)
        liste_des_meilleurs_scores.append(moy/nb_occurences)
    return liste_des_meilleurs_scores


# paramètres de la simulation :
fct_evaluation = evaluation.evaluation_dist_orient(1, 100)
fct_selection = selection.tournoi
fct_croisement = croisement.couple_aleatoire
proba_mutation = 0.001
fct_mutation = mutation.aleatoire(proba_mutation)
taille_population = 200
g_max_simulation = 2**32
nb_generations = 150
parametres_arret = [50, 0.01]

resultat = AlgoGenetique(fct_evaluation, fct_selection, fct_croisement, fct_mutation, 
                        taille_population, g_max_simulation, nb_generations, parametres_arret=parametres_arret)