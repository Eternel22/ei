from algorithm.algo_gen import Gene, Individu, Generation, AlgoGenetique
import algorithm.evaluation as evaluation
import algorithm.croisement as croisement
import algorithm.mutation as mutation
import algorithm.selection as selection

import os

# Localisation du dossier principal
main_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

"""

====== Test de la classe Gène =======

"""
def test_gene_init():
    """Test d'un gène"""
    test = Gene(0, 10, 5, 1)

    assert test.x_min == 0
    assert test.x_max == 10
    assert test.g_max == 5
    assert test.g == 1
    assert test.x == 2
    assert test.get_parameter() == test.x

    assert test.get_binary_rep() == '1'
    assert test.get_bit(0) == 1
    assert test.get_nb_bits() == 3

def test_gene_bit():
    """Test de la modification de bits"""
    test = Gene(0, 10, 5, 1)

    test.set_bit(1, 1)
    assert test.get_binary_rep() == '11'
    assert test.g == 3
    assert test.x == 6
    assert test.get_parameter() == test.x
    
    assert test.get_bit(0) == 1
    assert test.get_bit(1) == 1
    assert test.get_bit(2) == 0
    assert test.get_bit(3) == 0
    
    test.set_bit(0, 0)
    assert test.get_binary_rep() == '10'
    assert test.g == 2
    assert test.x == 4
    assert test.get_parameter() == test.x

    assert test.get_bit(0) == 0
    assert test.get_bit(1) == 1
    assert test.get_bit(2) == 0
    assert test.get_bit(3) == 0

def test_gene_bit_max():
    """Test du dépassement de g_max"""
    test = Gene(0, 10, 5, 1)

    test.set_bit(4, 1)
    assert test.g == test.g_max

def test_get_bit():
    """
    teste si la fonction get_bit marche bien
    """
    gene = Gene(0, 1, 10, 0)
    assert gene.get_bit(0) == 0

def test_nb_bits():
    gene = Gene(0, 10, 2**3, 0)
    assert gene.get_nb_bits() == 4

"""

==== Test de la classe Individu ====

"""

def test_individu_init():
    chromosome = [[Gene(0, 10, 5, 1), Gene(0, 10, 5, 1), Gene(0, 10, 5, 1)] for i in range(Individu.nb_chromosomes)]
    indiv = Individu(chromosome, 10)
    assert indiv.chromosome == chromosome
    assert indiv.score == 10
def test_individu_getchromosome():
    chromosome = [[Gene(0, 10, 5, 1), Gene(0, 10, 5, 1), Gene(0, 10, 5, 1)] for i in range(Individu.nb_chromosomes)]
    indiv = Individu(chromosome, 10)
    assert indiv.get_chromosome('AA') == chromosome[0]
def test_individu_setscore():
    chromosome = [[Gene(0, 10, 5, 1), Gene(0, 10, 5, 1), Gene(0, 10, 5, 1)] for i in range(Individu.nb_chromosomes)]
    indiv = Individu(chromosome, 10)
    indiv.set_score(0)
    assert indiv.score == 0

test_individu_init()
test_individu_getchromosome()
test_individu_setscore()




"""

==== Generation ====

"""

def test_generation_init():
    chromosome = [[Gene(0, 10, 5, 1), Gene(0, 10, 5, 1), Gene(0, 10, 5, 1)] for i in range(Individu.nb_chromosomes)]
    indiv = Individu(chromosome, 10)
    gen = Generation(10, [indiv])

    assert gen.id == 10
    assert gen.population[0] == indiv
    assert gen.moyenne == None
    assert gen.minimum == None
    assert gen.minimum_individu == None
    assert gen.maximum == None
    assert gen.maximum_individu == None

test_generation_init()


"""

==== AlgoGenetique ====

"""
def test_algogen_init():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 10, plasmid_path, 2, [10, 0.1], False)
    assert test.nb_generations_max == 10
    assert test.nb_generations == 10
    assert len(test.generations) == 10
    assert test.finished == True
    assert test.parametres_arret == [10, 0.1]
    assert test.plasmid_path == plasmid_path
    assert test.bases_fictives == 2

    lineList = [line.rstrip('\n') for line in open(plasmid_path)]
    plasmid_seq = ''.join(lineList[1:])
    plasmid_seq += plasmid_seq[:2]

    assert test.plasmid_seq == plasmid_seq
    assert test.best_individu.score == min([generation.minimum for generation in test.generations])

def test_algogen_createfirst():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 10, plasmid_path, 2, [10, 0.1], False)
    
    test.create_first_generation(10, 2**32)
    firstgen = test.get_current_generation()
    assert firstgen.id == 1
    assert len(firstgen.population) == 10

    for individu in firstgen.population:
        for dinucleotide, values in AlgoGenetique.rot_table_original.getTable().items():
            indivValues = individu.get_chromosome(dinucleotide)

            for iGene in range(3):
                coef = 1
                if dinucleotide in Individu.ATCG_specials and iGene == 2:
                    coef = -1
                assert coef*indivValues[iGene].x_min == values[iGene] - values[iGene+3]
                assert coef*indivValues[iGene].x_max == values[iGene] + values[iGene+3]

def test_algogen_getRotTable():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [10, 0.1], False)
    
    indiv = test.best_individu

    rotTable = AlgoGenetique.getRotTable(None, indiv)
    
    for key in Individu.ATCG_to_id:
        assert key in rotTable.getTable()

    for dinucleotide in rotTable.getTable():
        assert dinucleotide in Individu.ATCG_to_id
        genes = indiv.get_chromosome(dinucleotide)
        assert rotTable.getTwist(dinucleotide) == genes[0].x
        assert rotTable.getWedge(dinucleotide) == genes[1].x
        
        coef = 1
        if dinucleotide in Individu.ATCG_specials:
            coef = -1
        assert rotTable.getDirection(dinucleotide) == coef*genes[2].x

        assert rotTable.getTwist(dinucleotide) >= genes[0].x_min
        assert rotTable.getTwist(dinucleotide) <= genes[0].x_max
        assert rotTable.getWedge(dinucleotide) >= genes[1].x_min
        assert rotTable.getWedge(dinucleotide) <= genes[1].x_max

def test_algogen_getTraj():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [10, 0.1], False)
    
    indiv = test.best_individu

    x, y, z = test.getTraj(indiv)
    assert len(x) == len(y)
    assert len(y) == len(z)
    assert isinstance(x[0], float)
    assert isinstance(y[0], float)
    assert isinstance(z[0], float)

def test_algogen_current_generation():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [10, 0.1], False)
    
    assert test.get_current_generation() == test.generations[-1]

def test_algogen_add_generation():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [10, 0.1], False)
    
    gen = Generation(10, [])
    test.add_generation(gen)
    assert test.get_current_generation() == gen

def test_algogen_copyGene():
    gene = Gene(0, 1, 10, 0)
    gene_copie = AlgoGenetique.copyGene(None, gene)
    assert gene.x_min == gene_copie.x_min and gene.x_max == gene_copie.x_max and gene.g_max == gene_copie.g_max and gene.g == gene_copie.g

    gene.set_bit(0, 1)
    assert gene.x != gene_copie.x

def test_algogen_copyIndividu():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [10, 0.1], False)
    
    indiv = test.best_individu

    nouv = test.copyIndividu(indiv)

    assert indiv != nouv

def test_algogen_copyGeneration():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [10, 0.1], False)
    
    gen = test.copyGeneration(test.get_current_generation())
    assert gen != test.get_current_generation()

def test_algogen_enregistrement():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [10, 0.1], False)
    
    files = os.listdir(f"{main_path}/3dna/datas")

    test.enregistrement()

    nouv_files = os.listdir(f"{main_path}/3dna/datas")

    assert len(nouv_files) == len(files) + 2

    for file in nouv_files:
        if file not in files:
            os.remove(f"{main_path}/3dna/datas/{file}")

def test_algogen_stagnationmin():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 3, plasmid_path, 2, [1, 0.1], False)
    
    assert test.nb_generations == 2

def test_algogen_stagnationmoy():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 10, plasmid_path, 2, [100, 0.1], False)
    for gen in test.generations:
        gen.moyenne = 1
        gen.mininimum = 0
    test.generations[2].minimum = -1
    test.generations[2].moyenne = 0.99
    test.parametres_arret = [8, 0.1]
    assert test.is_finished(test.generations[0]) == True


"""

Test des croisements

"""

def test_croisement_melange_genes_2pts():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    

    gene1 = Gene(0, 10, 2**3, 1)
    gene2 = Gene(0, 10, 2**3, 4)

    nouv1, nouv2 = croisement.melange_genes_2pts(test, gene1, gene2)
    assert nouv1.x_min == 0
    assert nouv2.x_min == 0
    assert nouv1.x_max == 10
    assert nouv2.x_max == 10
    assert nouv1.g <= 2**3
    assert nouv2.g <= 2**3
    assert (nouv1.g | nouv2.g) == (gene1.g | gene2.g)
    assert (nouv1.g & nouv2.g) == (gene1.g & gene2.g)

def test_croisement_melange_genes_plus():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    

    gene1 = Gene(0, 10, 2**3, 1)
    gene2 = Gene(0, 10, 2**3, 4)
    gene3 = Gene(0, 10, 2**3, 7)

    nouv = croisement.melange_genes_plus_de_2_parents(test, [gene1, gene2, gene3])
    assert nouv.x_min == 0
    assert nouv.x_min == 0
    assert nouv.g_max == 2**3
    assert nouv.g <= 2**3

def test_croisement_reproduction():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    indiv1, indiv2 = test.generations[0].population[0], test.generations[0].population[1]

    enfant1, enfant2 = croisement.reproduction(test, indiv1, indiv2)

    for iChromosome in range(len(indiv1.chromosome)):
        for i in range(2):
            gene_enfant1 = enfant1.chromosome[iChromosome][i]
            gene_enfant2 = enfant2.chromosome[iChromosome][i]
            gene_parent1 = indiv1.chromosome[iChromosome][i]
            gene_parent2 = indiv2.chromosome[iChromosome][i]
            assert gene_enfant1.g & gene_enfant2.g == gene_parent1.g & gene_parent2.g

def test_croisement_reproduction_2parents():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    indiv1, indiv2 = test.generations[0].population[0], test.generations[0].population[1]

    enfant = croisement.reproduction_plus_de_2_parents(test, [indiv1, indiv2])
    assert len(enfant.chromosome) == len(indiv1.chromosome)

def test_croisement_couple_aleatoire():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    croisement.couple_aleatoire(test, test.generations[0])
    assert 20 == len(test.generations[0].population)

def test_croisement_orgie():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    fct = croisement.orgie(5)

    fct(test, test.generations[0])
    assert 20 == len(test.generations[0].population)


"""
Test évaluation
"""
def test_evaluation_simple():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    evaluation.evaluation_simple(test, test.get_current_generation())
    for indiv in test.get_current_generation().population:
        assert indiv.score != None

def test_evaluation_dist_orient():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_dist_orient(1, 100), fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    fct = evaluation.evaluation_dist_orient(1, 100)

    fct(test, test.get_current_generation())
    for indiv in test.get_current_generation().population:
        assert indiv.score != None

def test_evaluation_volume():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.volume, fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    fct = evaluation.volume

    fct(test, test.get_current_generation())
    for indiv in test.get_current_generation().population:
        assert indiv.score != None

"""
Test des mutations
"""
def test_mutation_aleatoire():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_simple, fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    fct = mutation.aleatoire(0.001)
    n = len(test.get_current_generation().population)

    fct(test, test.get_current_generation())
    assert n == len(test.get_current_generation().population)


def test_mutation_petitsbits():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_simple, fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    fct = mutation.petits_bits(0.001, 10)

    n = len(test.get_current_generation().population)
    fct(test, test.get_current_generation())
    assert n == len(test.get_current_generation().population)

def test_mutation_generation():

    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_simple, fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    fct = mutation.generation(0.001, 1, 10, 10)

    n = len(test.get_current_generation().population)
    fct(test, test.get_current_generation())
    assert n == len(test.get_current_generation().population)

"""
Test des fonctions de sélection
"""
def test_selection_tournoi():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_simple, fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    n = len(test.get_current_generation().population)
    fct = selection.tournoi
    fct(test, test.get_current_generation())
    assert n//2 == len(test.get_current_generation().population)

def test_selection_tournoi_aleatoire_fixe():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_simple, fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    n = len(test.get_current_generation().population)
    fct = selection.tournoi_aleatoire_fixe(0.1)
    fct(test, test.get_current_generation())
    assert n//2 == len(test.get_current_generation().population)


def test_selection_tournoi_aleatoire_dynamique():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_simple, fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    n = len(test.get_current_generation().population)
    fct = selection.tournoi_aleatoire_dynamique
    fct(test, test.get_current_generation())
    assert n//2 == len(test.get_current_generation().population)


def test_selection_elitisme():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_simple, fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    scores = sorted([individu.score for individu in test.get_current_generation().population])

    fct = selection.elitisme
    fct(test, test.get_current_generation())
    assert len(scores)//2 == len(test.get_current_generation().population)

    scores_nouv = sorted([individu.score for individu in test.get_current_generation().population])

    for i in range(len(scores_nouv)):
        assert scores_nouv[i] == scores[i]

def test_selection_roulette():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_simple, fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    n = len(test.get_current_generation().population)
    fct = selection.roulette
    fct(test, test.get_current_generation())
    assert n//2 == len(test.get_current_generation().population)

def test_selection_roulette_rang():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_simple, fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    n = len(test.get_current_generation().population)
    fct = selection.roulette_rang
    fct(test, test.get_current_generation())
    assert n//2 == len(test.get_current_generation().population)

def test_selection_roulette_rang_sansrepet():
    fct_inutile = lambda self, gen : ()
    plasmid_path = os.path.join(main_path, "data/plasmid_8k.fasta")
    test = AlgoGenetique(evaluation.evaluation_simple, fct_inutile, fct_inutile, fct_inutile, 10, 2**32, 2, plasmid_path, 2, [100, 0.1], False)
    
    n = len(test.get_current_generation().population)
    fct = selection.roulette_rang_sansrepet
    fct(test, test.get_current_generation())
    assert n//2 == len(test.get_current_generation().population)

