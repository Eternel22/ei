import numpy as np
import math


def evaluation_simple(self, generation):
    """
    Attribue un score à chaque individu d'une génération.
    Ce score est stocké dans l'instance de chaque individu : self.score

    Le score est égal à la distance entre les deux extrémités

    Paramètre :
    - generation: La génération à traiter
    """

    for individu in generation.population:
        x, y, z = self.getTraj(individu)
        x0, y0, z0 = x[0], y[0], z[0]
        xF, yF, zF = x[-1], y[-1], z[-1]

        distance = math.sqrt((x0-xF)**2 + (y0-yF)**2 + (z0-zF)**2)
        individu.set_score(distance)

def evaluation_dist_orient_coef(self, generation, coef1, coef2):
    """
    Attribue un score à chaque individu d'une génération

    Le score est calculé, après avoir ajouté 2 bases "fictives" à la fin de l'adn.
    Il correspond à coef1 * distance + coef2 * orientation
    """

    for individu in generation.population:
        x, y, z = self.getTraj(individu)
        
        dist2pts = math.sqrt((x[0]-x[-2])**2 + (y[0]-y[-2])**2 + (z[0]-z[-2])**2)

        vect1 = [(x[1]-x[0]),  (y[1]-y[0]), (z[1]-z[0])]
        vect2 = [(x[-1]-x[-2]), (y[-1]-y[-2]), (z[-1]-z[-2])]

        distOrient = math.sqrt((vect1[0]-vect2[0])**2 + (vect1[1]-vect2[1])**2 + (vect1[2]-vect2[2])**2)

        #print(">", dist2pts, distOrient)

        individu.set_score(dist2pts + distOrient*100)

def evaluation_dist_orient(coef1 = 1, coef2 = 100):
    """Retourne evaluation_dist_orient_coef, avec les coef1 et coef2"""
    return lambda self, generation : evaluation_dist_orient_coef(self, generation, coef1, coef2)


def volume(self, generation):
    """
    Attribue un score a chaque individu d'une génération.
    Ce score est stocké dans l'instance de chaque individu : self.score

    Le score est égal à une estimation du volume occupé par la molécule d'ADN
    Le volume calculé correspond au volume du plus petit parallélépidède rectangle dans lequel la molécule rentre

    Paramètre :
    - generation: La génération à traiter
    """
    for individu in generation.population:
        x, y, z = self.getTraj(individu)
        xmin, ymin, zmin = min(x), min(y), min(z)
        xmax, ymax, zmax = max(x), max(y), max(z)

        volume = (xmax - xmin)*(ymax - ymin)*(zmax - zmin)
        individu.set_score(volume)
