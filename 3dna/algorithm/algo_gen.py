from numpy.random import randint
from numpy.random import default_rng
from RotTable import RotTable
from Traj3D import Traj3D

import math
import numpy as np
import time

import matplotlib.pyplot as plt

import os, json


# Localisation du dossier principal
main_path = os.path.abspath(os.path.dirname(
    os.path.dirname(os.path.dirname(__file__))))


class Gene:
    """ 
    Classe d'un gène, qui correspond à la valeur d'un paramètre

    ## Variables de chaque instance :
    - x_min (float) : la valeur minimale du paramètre
    - x_max (float) : la valeur maximale du paramètre
    - g_max (int) : la valeur maximale de l'entier qui code le paramètre, qui correspond au nombre de valeurs que peut prendre le paramètre
    - g (int) : l'entier qui code le paramètre
    - x (float): la valeur du paramètre

    ## Fonctions de chaque instance :
    - get_parameter
    - get_bit
    - set_bit
    - get_nb_bits
    - get_binary_rep
    """

    def __init__(self, x_min: float, x_max: float, g_max: int, g: int):
        """
        Création d'un gène.
        Paramètres :
        - x_min (float) : la valeur minimale du paramètre
        - x_max (float) : la valeur maximale du paramètre
        - g_max (int) : la valeur maximale de l'entier qui code le paramètre,
                qui correspond au nombre de valeurs que peut prendre le paramètre
        - g (int) : l'entier qui code le paramètre
        """
        self.x_min = x_min
        self.x_max = x_max
        self.g_max = g_max
        self.g = g
        self.x = self.get_parameter()

    def get_parameter(self):
        """
        Retourne le paramètre x
        """
        return self.x_min + (self.g / self.g_max) * (self.x_max - self.x_min)

    def get_bit(self, i):
        """
        Retourne la valeur du ième bit de g (0 ou 1)

        Paramètres :
        - i : Le ième bit 
        """
        return (self.g >> i) & 1

    def set_bit(self, i, bit):
        """
        Transforme la valeur du iBit-ième de g en bit (0 ou 1)
        Si g dépasse g_max, il est ramené à g_max

        Paramètres :
        - i : le ième bit à traiter
        - bit : la valeur du bit (0 ou 1)
        """
        if bit == 1:
            self.g = self.g | (1 << i)
        elif bit == 0:
            self.g = self.g & ~(1 << i)

        if self.g > self.g_max:
            self.g = self.g_max

        self.x = self.get_parameter()

    def get_nb_bits(self):
        """
        Retourne le nombre de bits de g_max
        """
        return len("{0:b}".format(self.g_max))

    def get_binary_rep(self):
        """
        Retourne la représentation binaire de g (String)
        """
        return "{0:b}".format(self.g)


class Individu:
    """
    Un individu, qui possède plusieurs gènes.
    L'individu possède des chromosomes. Chaque chromosome possède 3 gènes.
    
    ## Variables globales :
    - nb_chromosomes : le nombre de chromosomes
    - ATCG_to_id : un dictionnaire qui associe à chaque dinucléotide, l'identifiant du chromosome auquel il appartient
    - ATCG_specials : un set qui contient les dinucléotides spéciales, dont les paramètres peuvent être déduites de d'autres nucléotides

    ## Variables de chaque instance :
    - chromosome : une liste de chromosomes. Chaque chromosome est une liste de 3 gènes, correspondants aux 3 paramètres 
    - score : le score de l'individu (None par défaut)

    ## Fonctions de chaque instance :
    - set_score
    - get_chromosome
    """
    nb_chromosomes = 10
    ATCG_to_id = {
        'AA': 0, 'TT': 0,
        'AC': 1, 'GT': 1,
        'AG': 2, 'CT': 2,
        'AT': 3,
        'CA': 4, 'TG': 4,
        'CC': 5, 'GG': 5,
        'CG': 6,
        'GA': 7, 'TC': 7,
        'GC': 8,
        'TA': 9
    }
    ATCG_specials = {'TT', 'GT', 'CT', 'TG', 'GG', 'TC'}

    def __init__(self, chromosome, score=None):
        """
        Paramètres :
        - chromosome : une liste de chromosomes. Chaque chromosome est une liste de 3 gènes, correspondants aux 3 paramètres 
        - score : le score (None par défaut)
        """

        self.chromosome = chromosome
        self.score = score

    def get_chromosome(self, dinucleotide):
        """
        Retourne le chromosome d'un dinucléotide (une liste de 3 gènes)

        Paramètres :
        - dinucleotide : une séquence de deux nucléotides ('AA', 'AC', ...)
        """
        return self.chromosome[Individu.ATCG_to_id[dinucleotide]]

    def set_score(self, score):
        """
        Met à jour le score de l'individu.

        Paramètres :
        - score
        """
        self.score = score


class Generation:
    """
    Une génération, qui contient plusieurs individus.
    
    ## Variables de chaque instance :
    - id : l'identifiant de la génération
    - population : une liste d'individus
    - moyenne : la moyenne des scores de la population (None par défaut)
    - minimum : le score minimum des individus de la population (None par défaut)
    - minimum_individu : l'individu avec le score minimum (None par défaut) 
    - maximum : le score maximum des individus de la population (None par défaut)
    - maximum_individu : l'individu avec le score maximum (None par défaut)
    """

    def __init__(self, id: int, population: list):
        """
        Paramètres :
        - id : l'identifiant de la génération
        - population : une liste d'individus
        """
        self.id = id
        self.population = population
        self.moyenne = None
        self.minimum = None
        self.minimum_individu = None
        self.maximum = None
        self.maximum_individu = None


class AlgoGenetique:
    """ 
    Classe responsable de l'évolution des générations.
    On cherchera à MINIMISER le score des individus

    ## Variables globales :
    - rot_table_original : une instance de RotTable
    - traj : une instance de Traj3D
    
    ## Variables de chaque instance :
    - nb_generations : le nombre de générations
    - nb_generations_max : le nombre de générations à effectuer
    - generations : une liste de générations
    - finished : si l'algorithme s'est terminé
    - plasmid_path : le chemin du plasmid
    - plasmid_seq : la séquence du plasmid
    - best_individu : le meilleur individu
    - bases_fictives : le nombre de bases fictives
    - plot_and_asksave : s'il faut afficher, et demander d'enregistrer à la fin

    ## Fonctions de chaque instance :
    - create_first_generation : Créé la première génération
    - getRotTable : Obtenir la RotTable d'un individu
    - getTraj : Calcule la trajectoire du plasmid, pour un individu donné
    - get_current_generation : Retourne la dernière génération
    - add_generation : Ajoute une génération
    - copyGene
    - copyIndividu
    - copyGeneration

    ## Fonctions internes :
    - evolution : Effectue l'évolution des générations
    - affiche_temps : Fonction interne
    - affiche_infos : Fonction interne
    - update_scores : Fonction interne
    - is_finished : Fonction interne
    - finale : Fonction interne
    - enregistrement : Fonction interne
    """

    # RotTable original, sans modification
    rot_table_original = RotTable()
    traj = Traj3D()

    def __init__(self, fct_evaluation, fct_selection, fct_croisement, fct_mutation,
                 population_init: int, g_max: int = 2**32, nb_generations_max: int = 15,
                 plasmid_path: str = os.path.join(
                     main_path, "data/plasmid_8k.fasta"), bases_fictives: int = 2,
                 parametres_arret=[10, .01], plot_and_asksave=True):
        """
        Initie une génération initiale

        Paramètres :
        - fct_evaluation : Une fonction d'évaluation, qui prend en paramètre : (l'instance de AlgoGenetique, la génération à traiter)
        - fct_selection : Une fonction de sélection, qui prend en paramètre : (l'instance de AlgoGenetique, la génération à traiter)
        - fct_croisement : Une fonction de croisement, qui prend en paramètre : (l'instance de AlgoGenetique, la génération à traiter)
        - fct_mutation : Une fonction de mutation, qui prend en paramètre : (l'instance de AlgoGenetique, la génération à traiter)
        - population_init : La taille de la population initiale
        - g_max : Le nombre de possibilités pour chaque paramètres (2^32 par défaut)
        - nb_generations_max : Le nombre de générations à effectuer
        - plasmid_path : Le chemin du plasmid (plasmid_8k par défaut)
        - bases_fictives : Le nombre de bases fictives à ajouter.
        - parametres_arret : Les paramètres d'arrêt (longueur de la stagnation, coefficient de stagnation)
        - plot_and_asksave : S'il faut afficher, et demander d'enregistrer à la fin
        """
        self.nb_generations = 0
        self.nb_generations_max = nb_generations_max
        self.generations = []
        self.finished = False
        self.parametres_arret = parametres_arret
        self.plasmid_path = plasmid_path
        self.bases_fictives = bases_fictives
        self.plot_and_asksave = plot_and_asksave

        # Création de plasmid_seq
        lineList = [line.rstrip('\n') for line in open(self.plasmid_path)]
        self.plasmid_seq = ''.join(lineList[1:])

        # On ajoute des bases fictives à la fin du plasmid. Ces bases correspondent aux premières bases du plasmid.
        self.plasmid_seq += self.plasmid_seq[:bases_fictives]
        
        # Par défaut, le meilleur individu est un individu sans chromosome, avec un score infini
        self.best_individu = Individu(dict(), math.inf)
        self.best_individu.set_score(math.inf)

        # Génération de la première génération
        self.create_first_generation(population_init, g_max)

        # On fait évoluer les générations
        while not self.finished:
            self.evolution(fct_evaluation, fct_selection,
                           fct_croisement, fct_mutation)

    def create_first_generation(self, population_init, g_max):
        """
        Créé la première génération, et l'ajoute au tableau self.generations.
        La première génération est constitué d'individus dont les paramètres ont été générés aléatoirement, suivant une loi uniforme.

        Paramètres :
        - population_init : la taille de la population initiale
        - g_max : la valeur de g_max, pour les paramètres
        """

        # On génère les valeurs de g au préalable, de manière uniforme pour chaque paramètre, et sans répétition
        randoms_g = []
        for iChromosome in range(Individu.nb_chromosomes):
            chromosome = []
            for iGene in range(3):
                #chromosome.append(randint(low=0, high=g_max, size=population_init))
                chromosome.append(default_rng().choice(g_max+1, size=population_init, replace=False))
            randoms_g.append(chromosome)

        # On créé les individus
        individus = []
        for iIndividu in range(population_init):
            chromosome = [None for i in range(Individu.nb_chromosomes)]
            for dinucleotide, values in AlgoGenetique.rot_table_original.getTable().items():
                # On ignore les dinucléotides spéciales, dont les paramètres peuvent être déduites de d'autres nucléotides
                if dinucleotide in Individu.ATCG_specials:
                    continue

                iChromosome = Individu.ATCG_to_id[dinucleotide]
                nouvGenes = []
                for iGene in range(3):
                    nouvGenes.append(Gene(values[iGene] - values[iGene + 3], values[iGene] +
                                     values[iGene + 3], g_max, randoms_g[iChromosome][iGene][iIndividu]))
                chromosome[iChromosome] = nouvGenes
            individus.append(Individu(chromosome))

        print("Generation 1")
        self.add_generation(Generation(1, individus))

    def getRotTable(self, individu: Individu):
        """
        Calcule la RotTable d'un individu

        Paramètres :
        - individu
        """
        rot_table = RotTable()

        for dinucleotide in rot_table.getTable():
            genes = individu.get_chromosome(dinucleotide)
            rot_table.setTwist(dinucleotide, genes[0].x)
            rot_table.setWedge(dinucleotide, genes[1].x)

            # Si la dinucléotide est spéciale, on peut déduire son paramètre d'un autre dinucléotide. Ici, en particulier, la direction doit être l'opposé.
            if dinucleotide in Individu.ATCG_specials:
                rot_table.setDirection(dinucleotide, -genes[2].x)
            else:
                rot_table.setDirection(dinucleotide, genes[2].x)

        return rot_table

    def getTraj(self, individu: Individu):
        """
        Calcule la trajectoire du plasmid
        Retourne 3 listes numpy : une liste x, y et z

        Paramètres :
        - individu
        """

        AlgoGenetique.traj.compute(
            self.plasmid_seq, self.getRotTable(individu))

        xyz = np.array(AlgoGenetique.traj.getTraj())

        return xyz[:, 0], xyz[:, 1], xyz[:, 2]

    def get_current_generation(self):
        """
        Retourne la dernière génération
        """
        return self.generations[-1]

    def add_generation(self, generation: Generation):
        """
        Ajoute une génération

        Paramètre :
        - generation
        """
        self.nb_generations += 1
        self.generations.append(generation)

    def evolution(self, fct_evaluation, fct_selection, fct_croisement, fct_mutation):
        """
        Evolue la dernière génération de self.generations, et ajoute cette nouvelle génération

        Paramètres :
        - fct_evaluation
        - fct_selection
        - fct_croisement
        - fct_mutation
        """

        # Dernière génération
        current_generation = self.get_current_generation()

        # Evaluation de la dernière génération (attribution d'un score à chaque individu)
        t0 = time.time()
        fct_evaluation(self, current_generation)
        self.update_scores(current_generation)
        t1 = time.time()
        self.affiche_infos(current_generation)

        # Duplication de la génération
        new_generation = self.copyGeneration(current_generation)
        new_generation.id += 1
        print(f"Generation {new_generation.id}")
        t2 = time.time()

        # Séléction
        fct_selection(self, new_generation)
        t3 = time.time()

        # Croisement
        fct_croisement(self, new_generation)
        t4 = time.time()

        # Mutation
        fct_mutation(self, new_generation)
        t5 = time.time()

        # Ajout de la nouvelle génération
        self.add_generation(new_generation)
        t6 = time.time()

        self.affiche_temps(t0, t1, t2, t3, t4, t5, t6)

        # On vérifie si l'on doit s'arrêter
        if self.is_finished(new_generation):
            self.finished = True
            fct_evaluation(self, new_generation)
            self.update_scores(new_generation)
            self.affiche_infos(new_generation)
            self.finale(new_generation)

    def affiche_temps(self, t0, t1, t2, t3, t4, t5, t6):
        print("  Temps de l'evaluation : ",
              "{:.2f}".format((t1 - t0)*1000), "ms")
        print("  Temps de la copie de la generation : ",
              "{:.2f}".format((t2 - t1)*1000), "ms")
        print("  Temps de la selection : ",
              "{:.2f}".format((t3 - t2)*1000), "ms")
        print("  Temps du croisement : ",
              "{:.2f}".format((t4 - t3)*1000), "ms")
        print("  Temps de la mutation : ",
              "{:.2f}".format((t5 - t4)*1000), "ms")
        print("  Temps de l'ajout de la generation : ",
              "{:.2f}".format((t6 - t5)*1000), "ms")

    def affiche_infos(self, generation: Generation):
        print(f"  Taille de la population : {len(generation.population)}")
        print(f"  Score min : {generation.minimum}")
        print(f"  Score moyen : {generation.moyenne}")

    def update_scores(self, generation: Generation):
        """
        Calcul de la moyenne, du minimum et du maximum des scores
        """
        generation.minimum = math.inf
        generation.moyenne = 0
        generation.maximum = -math.inf

        for individu in generation.population:
            if individu.score < generation.minimum:
                generation.minimum = individu.score
                generation.minimum_individu = individu
            if individu.score > generation.maximum:
                generation.maximum = individu.score
                generation.maximum_individu = individu
            generation.moyenne += individu.score

        generation.moyenne /= len(generation.population)

        if self.best_individu.score > generation.minimum:
            self.best_individu = generation.minimum_individu

    def is_finished(self, nouvelle_generation):
        """ Renvoie si l'évolution doit être considérée terminée ou non """
        if nouvelle_generation.id == self.nb_generations_max:
            print("Nombre de générations maximal atteint")
            return True
        stagne_min, moyenne_coeff_stagne = self.parametres_arret
        
        if len(self.generations) < stagne_min+1:
            return False

        liste_min = [gen.minimum for gen in self.generations][-stagne_min-1:-1]
        liste_moy = [gen.moyenne for gen in self.generations][-stagne_min-1:-1]

        if liste_min[0] == min(liste_min):
            print("Stagnation du minimum, arrêt")
            return True

        # Etude de la moyenne 
        maximum, minimum = max(liste_moy), min(liste_moy)
        moyenne = (maximum + minimum) / 2

        if maximum < moyenne * (1 + moyenne_coeff_stagne) or minimum > moyenne * (1 - moyenne_coeff_stagne):
            print("Stagnation de la moyenne, arrêt")
            return True
        
        return False

    def finale(self, generation: Generation):
        """ Définit les actions en fin de simulation """

        print("== [ Résultats ] ==")
        print(f'Score atteint : {self.best_individu.score}')

        # Affichage des paramètres
        bestRotTable = self.getRotTable(self.best_individu)
        for key in AlgoGenetique.rot_table_original.getTable():
            print(key, ")", bestRotTable.getTwist(key),
                  bestRotTable.getWedge(key), bestRotTable.getDirection(key))

        # Affichage de la trajectoire
        x, y, z = self.getTraj(self.best_individu)

        plt.close()

        ax = plt.axes(projection='3d')

        ax.plot(x, y, z)

        bf = self.bases_fictives

        # Points au début, et premier point fictif (normalement aux mêmes endroits)
        ax.plot(x[0], y[0], z[0], "g.")
        ax.plot(x[-bf], y[-bf], z[-bf], "r.")

        # Point juste après le début, et juste avant la fin
        ax.plot(x[1], y[1], z[1], "g+")
        ax.plot(x[-bf-1], y[-bf-1], z[-bf-1], "r+")

        # Vecteur donnant la direction
        if bf >= 2:
            ax.plot([x[0], x[0] + (x[1]-x[0])*20], [y[0],y[0] + (y[1]-y[0])*20], 'g', zs=[z[0],z[0]+ (z[1]-z[0])*20])
            ax.plot([x[-bf], x[-bf] + (x[-bf+1]-x[-bf])*20], [y[-bf],y[-bf] + (y[-bf+1]-y[-bf])*20], 'r', zs=[z[-bf],z[-bf]+ (z[-bf+1]-z[-bf])*20])

        # Points inutiles, pour centrer la figure
        ax.plot(-5000, -5000, -5000, "w+")
        ax.plot(5000, 5000, 5000, "w+")

        plt.figure()

        # Affichage des minimums et des moyennes
        liste_min = [gen.minimum for gen in self.generations]
        liste_moy = [gen.moyenne for gen in self.generations]

        plt.plot(liste_min, label='Minimums')
        plt.plot(liste_moy, label="Moyennes")
        plt.legend()

        if(self.plot_and_asksave):
            plt.show()

            if input("Enregistrer ? (yes / anything) ") == 'yes':
                self.enregistrement()

    def enregistrement(self):
        """ 
        Enregistre les données d'une simulation dans deux fichier, présents dans le dossier 3dna/datas :
        - un fichier txt, où le score, les paramètres et les positions du plasmid sont sauvegardés
        - un fichier json, où les paramètres sont sauvegardés
        """
        texte = '---\n'
        texte += f'Score atteint : {self.best_individu.score}\n'

        bestRotTable = self.getRotTable(self.best_individu)
        for key in AlgoGenetique.rot_table_original.getTable():
            texte += key + ")" + str(bestRotTable.getTwist(key)) + ' / ' + \
                str(bestRotTable.getWedge(key)) + ' / ' + \
                str(bestRotTable.getDirection(key))
            texte += '\n'

        texte += '---\n'
        x, y, z = self.getTraj(self.best_individu)
        for i, elt in enumerate(x):
            texte += f'{elt} / {y[i]} / {z[i]}\n'

        os.makedirs(f"{main_path}/3dna/datas", exist_ok=True)

        temps = int(time.time())

        with open(f'{main_path}/3dna/datas/datas{temps}.txt', 'w', encoding='utf8') as file:
            file.write(texte)
        
        for dinucleotide in bestRotTable.getTable():
            for i in range(3):
                bestRotTable.getTable()[dinucleotide][3+i] = 0
        with open(f'{main_path}/3dna/datas/datas{temps}.json', 'w') as file:
            json.dump(bestRotTable.getTable(), file, sort_keys=True, indent=4)

    def copyGene(self, gene: Gene):
        """
        Copier un gène

        Paramètre :
        - gene
        """
        nouv = Gene(gene.x_min, gene.x_max, gene.g_max, gene.g)
        return nouv

    def copyIndividu(self, individu: Individu):
        """
        Copier un individu

        Paramètre :
        - individu
        """
        nouvChromosome = []
        for values in individu.chromosome:
            nouvChromosome.append([self.copyGene(values[0]), self.copyGene(values[1]), self.copyGene(values[2])])
        nouv = Individu(nouvChromosome, individu.score)
        return nouv

    def copyGeneration(self, generation: Generation):
        """
        Copier une génération

        Paramètre :
        - generation
        """
        nouv = Generation(generation.id, [self.copyIndividu(individu) for individu in generation.population])
        nouv.moyenne = generation.moyenne
        nouv.minimum = generation.minimum
        nouv.minimum_individu = generation.minimum_individu
        nouv.maximum = generation.maximum
        nouv.maximum_individu = generation.maximum_individu
        return nouv