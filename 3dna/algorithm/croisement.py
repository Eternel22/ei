from numpy.random import default_rng
from numpy.random import randint


def melange_genes_2pts(self, gene1, gene2):
    """
    Renvoie deux nouveaux genes avec la methode de croisement en 2 points
    
    Paramètres :
    - gene1
    - gene2
    """

    point1 = randint(0, gene1.get_nb_bits())
    point2 = randint(0, gene1.get_nb_bits())

    maxi = max(point1, point2)
    mini = min(point1, point2)

    nouveau_gene1 = self.copyGene(gene1)
    nouveau_gene2 = self.copyGene(gene2)

    for iBit in range(mini, maxi):
        nouveau_gene1.set_bit(iBit, gene2.get_bit(iBit))
        nouveau_gene2.set_bit(iBit, gene1.get_bit(iBit))

    return nouveau_gene1, nouveau_gene2


def melange_genes_plus_de_2_parents(self, ls_gene):
    """
    Renvoie un gene avec la methode de croisement en 2 points pour des couples à plusieurs parents
    Prend aléatoirement un couple parmi les gènes donnés en entrée, et fait un enfant

    Paramètres :
    - ls_gene : la liste des gènes
    """

    nb_parents = len(ls_gene)
    gene1 = ls_gene[0]

    point1 = randint(0, gene1.get_nb_bits())
    point2 = randint(0, gene1.get_nb_bits())

    maxi = max(point1, point2)
    mini = min(point1, point2)

    numbers = default_rng().choice(nb_parents, size=2, replace=False)

    nb_parent1 = numbers[0]
    nb_parent2 = numbers[1]

    gene1 = ls_gene[nb_parent1]
    gene2 = ls_gene[nb_parent2]

    nv_gene = self.copyGene(gene1)

    for iBit in range(mini, maxi):
        nv_gene.set_bit(iBit, gene2.get_bit(iBit))

    return nv_gene


def reproduction(self, parent1, parent2):
    """
    Donne 2 enfants a partir de 2 parents

    Paramètres :
    - parent1
    - parent2
    """

    enfant1, enfant2 = self.copyIndividu(parent1), self.copyIndividu(parent2)

    for iChromosome in range(len(parent1.chromosome)):
        for i in range(2):
            gene1 = parent1.chromosome[iChromosome][i]
            gene2 = parent2.chromosome[iChromosome][i]

            enfant1.chromosome[iChromosome][i], enfant2.chromosome[iChromosome][i] = melange_genes_2pts(
                self, gene1, gene2)

    return enfant1, enfant2


def reproduction_plus_de_2_parents(self, ls_parents):
    """
    Donne un enfant a partir d'une liste de parents

    Paramètre :
    - ls_parents : une liste de parents
    """

    nb_parents = len(ls_parents)
    parent1 = ls_parents[0]
    enfant = self.copyIndividu(parent1)

    for iChromosome in range(len(parent1.chromosome)):
        for i in range(2):
            ls_gene = []
            for parent in ls_parents:
                ls_gene.append(parent.chromosome[iChromosome][i])

            enfant.chromosome[iChromosome][i] = melange_genes_plus_de_2_parents(
                self, ls_gene)

    return enfant


def couple_aleatoire(self, generation):
    """ 
    Croisement aléatoire des individus, par la méthode des 2 pts

    Si la taille de la population est paire, la taille est préservée
    Sinon, un des individus est exclu.

    Paramètre :
    - generation : la génération à traiter
    """

    population = generation.population

    partenaire_possibles = [i for i in range(len(population))]
    couples = []

    # creation des n/2 couples
    while len(partenaire_possibles) > 1:

        rng = default_rng()
        numbers = rng.choice(len(partenaire_possibles),
                             size=2, replace=False)

        couples.append((population[partenaire_possibles[numbers[0]]],
                       population[partenaire_possibles[numbers[1]]]))
        partenaire_possibles.pop(max(numbers[0], numbers[1]))
        partenaire_possibles.pop(min(numbers[0], numbers[1]))

    # creation des n/2 nouveaux individus de la generation
    for couple in couples:

        parent1, parent2 = couple
        enfant1, enfant2 = reproduction(self, parent1, parent2)

        generation.population.append(enfant1)
        generation.population.append(enfant2)


def orgie_nbparents(self, generation, nb_parents):
    """
    Cette fonction prend n/2 fois au hasard un nombre nb_parents d'individus dans la population, et leur fait faire un enfant
    n la taille de la population

    Paramètre :
    - generation : La génération à traiter
    - nb_parents : Le nombre de parents à choisir
    """
    population = generation.population
    taille_pop = len(population)

    for i in range(taille_pop):
        ls_nb_parents = default_rng().choice(taille_pop, size=nb_parents, replace=False)
        ls_parents = [population[i] for i in ls_nb_parents]

        enfant = reproduction_plus_de_2_parents(self, ls_parents)
        generation.population.append(enfant)

def orgie(nb_parents = 5):
    """
    Cette fonction renvoie la fonction orgie_nbparents, avec un nb de parents fixés
    """
    return lambda self, generation : orgie_nbparents(self, generation, nb_parents)