from numpy.random import randint, random
from numpy.random import default_rng


def tournoi(self, generation):
    """ 
    Selection des individus, par un tournoi.

    Paramètre :
    - generation : La génération à traiter
    """

    n = len(generation.population)

    for i in range((n//2)):

        rng = default_rng()
        numbers = rng.choice(len(generation.population), size=2, replace=False)

        player1 = numbers[0]
        player2 = numbers[1]

        individu1, individu2 = generation.population[player1], generation.population[player2]

        if individu1.score < individu2.score:
            generation.population.pop(player2)
        else:
            generation.population.pop(player1)


def tournoi_aleatoire_fixe_proba(self, generation, proba_inverse):
    """ 
    Selection des individus, par un tournoi. On garde une chance pour que l'autre gagne aussi.

    Paramètre :
    - generation : La génération à traiter
    """

    n = len(generation.population)

    for i in range((n//2)):

        rng = default_rng()
        numbers = rng.choice(len(generation.population), size=2, replace=False)

        player1 = numbers[0]
        player2 = numbers[1]

        individu1, individu2 = generation.population[player1], generation.population[player2]

        if individu1.score < individu2.score or random() < proba_inverse:
            generation.population.pop(player2)
        else:
            generation.population.pop(player1)

def tournoi_aleatoire_fixe(proba):
    """Retourne tournoi_aleatoire_fixe_proba, avec une proba fixé"""
    return lambda self, generation : tournoi_aleatoire_fixe_proba(self, generation, proba)

def tournoi_aleatoire_dynamique(self, generation):
    """ 
    Selection des individus, par un tournoi.
    Plus la différence de score des deux individus divisée par la différence de score maximale possible, plus l'invidu avec le score le plus élevé a de chances de gagner

    Paramètre :
    - generation : La génération à traiter
    """
    proba_inverse = 0.1
    min_score = generation.minimum
    max_score = generation.maximum

    n = len(generation.population)

    for i in range((n//2)):

        rng = default_rng()
        numbers = rng.choice(len(generation.population), size=2, replace=False)

        player1 = numbers[0]
        player2 = numbers[1]

        individu1, individu2 = generation.population[player1], generation.population[player2]

        proba_inverse = 0.5 * \
            (1 + abs(individu1.score - individu2.score)/(max_score - min_score))

        if individu1.score < individu2.score or random() < proba_inverse:
            generation.population.pop(player2)
        else:
            generation.population.pop(player1)


def elitisme(self, generation):
    """
    Elitisme : on trie les individus par leur score, et on prend les n/2 meilleurs
    """
    classement = sorted(generation.population,
                        key=lambda individu: individu.score)
    generation.population = classement[:len(classement)//2]


def roulette(self, generation):
    """
    Roulette : On choisit les individus par un tirage aléatoire, en considérant que 
    plus un individu est performant, plus il a de chances d'être sélectionnés

    La "chance" pour un individu d'être sélectionné, est de -x + max des scores + epsilon

    Un même individu peut être sélectionné plusieurs fois !
    """
    n = len(generation.population)

    max_score = max([individu.score for individu in generation.population])

    epsilon = 1
    chances = [(-individu.score + max_score + epsilon)
               for individu in generation.population]

    somme = sum(chances)
    chances = [chance / somme for chance in chances]

    cumul = [chances[0]]
    for i in range(1, n):
        cumul.append(cumul[-1] + chances[i])

    nouvelle_population = []
    for iTirage in range(n//2):
        r = random()

        choix = n - 1
        for i in range(n):
            if r < cumul[i]:
                choix = i
                break

        nouvelle_population.append(generation.population[choix])

    generation.population = nouvelle_population


def roulette_rang(self, generation):
    """
    Roulette : On choisit les individus par un tirage aléatoire, en considérant que 
    plus un individu est performant, plus il a de chances d'être sélectionné

    La "chance" pour un individu d'être sélectionné, est proportionnelle à n - rang

    Un même individu peut être sélectionné plusieurs fois !
    """
    n = len(generation.population)

    classement_to_id = sorted(
        range(n), key=lambda id: generation.population[id].score)

    id_to_classement = [None for i in range(n)]
    for classement in range(n):
        id_to_classement[classement_to_id[classement]] = classement

    chances = [n - id_to_classement[id] for id in range(n)]

    somme = sum(chances)
    chances = [chance / somme for chance in chances]

    cumul = [chances[0]]
    for i in range(1, n):
        cumul.append(cumul[-1] + chances[i])

    nouvelle_population = []
    for iTirage in range(n//2):
        r = random()

        choix = n - 1
        for i in range(n):
            if r < cumul[i]:
                choix = i
                break

        nouvelle_population.append(generation.population[choix])

    generation.population = nouvelle_population


def roulette_rang_sansrepet(self, generation):
    """
    Roulette : On choisit les individus par un tirage aléatoire, en considérant que 
    plus un individu est performant, plus il a de chances d'être sélectionné

    La "chance" pour un individu d'être sélectionné, est proportionnelle à (n - rang)**2

    Un même individu NE PEUT PAS être sélectionné plusieurs fois !
    """
    n = len(generation.population)

    classement_to_id = sorted(
        range(n), key=lambda id: generation.population[id].score)

    id_to_classement = [None for i in range(n)]
    for classement in range(n):
        id_to_classement[classement_to_id[classement]] = classement

    chances = [(n - id_to_classement[id])**2 for id in range(n)]

    nouvelle_population = []
    for iTirage in range(n//2):

        somme = sum(chances)
        chances = [chance / somme for chance in chances]

        cumul = [chances[0]]
        for i in range(1, n):
            cumul.append(cumul[-1] + chances[i])

        r = random()

        choix = n - 1
        for i in range(n):
            if r < cumul[i]:
                choix = i
                break

        nouvelle_population.append(generation.population[choix])
        chances[choix] = 0

    generation.population = nouvelle_population
