from numpy.random import random
from math import floor


def aleatoire_proba(self, generation, proba_mutation):
    """ 
    Génère des mutations avec une probabilité uniforme sur tous les bits
    """
    population = generation.population
    for individu in population:  # on itère sur les individus
        chromosome = individu.chromosome
        for iChromosome in range(len(chromosome)):
            for nb_gene in range(2):
                gene = chromosome[iChromosome][nb_gene]
                # on itère sur les bits du gène
                for nb_bit in range(gene.get_nb_bits()):
                    p = random()
                    if p < proba_mutation:  # on modifie le bit avec la proba proba_mutation
                        gene.set_bit(nb_bit, (gene.get_bit(nb_bit)+1) % 2)

# permet d'avoir des fonctions qui ne prennent en argument que proba_mutation


def aleatoire(proba_mutation):
    """Retourne la fonction aleatoire_proba, avec proba_mutation"""
    return lambda self, generation: aleatoire_proba(self, generation, proba_mutation)


def mutation_petits_bits(self, generation, proba_mutation, rapport_mutation):
    """
    Génère des mutations 
    La probabilité est linéaire en fonction de la place du bit dans le gène
    Plus le bit est 'petit', plus il a de probabilité d'être changé

    Paramètres : 
    - generation : La génération à traiter
    - rapport_mutation : le rapport entre la proba de mutation du premier bit et la proba de mutation du dernier bit
    - proba_mutation : la somme de toutes les probas (aire sous la courbe de la fonction affine définissant la proba)
    """
    population = generation.population
    nb_bits = population[0].chromosome[0][0].get_nb_bits()

    def proba(x):
        a = (1-rapport_mutation)/(rapport_mutation+1) * \
            2*proba_mutation/nb_bits
        b = 2*proba_mutation/(rapport_mutation+1)
        return a*x + rapport_mutation*b

    for individu in population:  # on itère sur les individus
        chromosome = individu.chromosome
        for nucleotide in chromosome:
            # on itère sur les gènes présents dans le dictionnaire chromosome
            for nb_gene in range(2):
                gene = nucleotide[nb_gene]
                # on itère sur les bits du gène
                for iBit in range(nb_bits):
                    p = random()
                    if p < proba(iBit):  # on modifie le bit avec la proba associée à l'indice du bit
                        gene.set_bit(iBit, (gene.get_bit(iBit)+1) % 2)


def petits_bits(proba_mutation, rapport_mutation):
    """
    Retourne la fonction mutation_petits_bits, avec proba_mutation et rapport_mutation
    """
    return lambda self, generation: mutation_petits_bits(self, generation, proba_mutation, rapport_mutation)


def mutation_generation(self, generation, proba_mutation, rapport_mutation, rapport_generation, nb_generations):
    """
    On évolue de manière linéaire de rapport_mutation à rapport_generation
    """
    id = generation.id
    rapport = (rapport_mutation*(rapport_generation - 1) /
               nb_generations)*id + rapport_mutation
    mutation_petits_bits(self, generation, proba_mutation, rapport)


def generation(proba_mutation, rapport_mutation, rapport_generation, nb_generation):
    """
    Retourne mutation_generation
    """
    return lambda self, generation: mutation_generation(self, generation, proba_mutation, rapport_mutation, rapport_generation, nb_generation)
