import matplotlib.pyplot as plt

# Le fichier dont on veut extraire les données
FICHIER = 'datas/datas1674656162.txt'

with open(FICHIER, 'r', encoding='utf8') as file:
    lignes = file.readlines()
    compteur_demarquation = 0
    debut_lecture = False
    xyz = [[], [], []]
    for ligne in lignes:
        if debut_lecture:
            if ligne == '\n':
                break
            ligne = ligne.strip()
            ligne = ligne.split(' / ')

            for i, elt in enumerate(ligne):
                xyz[i].append(float(elt))

        if '---' in ligne:
            compteur_demarquation += 1
            if compteur_demarquation == 2:
                debut_lecture=True

x, y, z = xyz
ax = plt.axes(projection='3d')
ax.plot(x, y, z)
ax.plot(x[0], y[0], z[0], "r+")
ax.plot(x[-1], y[-1], z[-1], "g+")
plt.show()
            